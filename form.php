
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <title>Document</title>
    <style>
        .error {
            border: 2px solid red;
        }
    </style>
</head>
<body>

<?php
if (!empty($messages)) {
    print('<div id="messages">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-7">
            <h2>Форма</h2>
            <form action="index.php" method="POST">
                <label>
                    Ваше имя: <br />
                    <input name="fio" type="text" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" /> </label><br />

                <label>
                    Ваш email:<br />
                     <input name="email" type="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"/></label><br />


                <label>Введите свой год рождения</label><br>
                <select name="birthyear" <?php if ($errors['birthyear']) {print 'class="error"';} ?> value="<?php print $values['birthyear']; ?>">
                    <?php for ($i = 1900; $i < 2022; $i++) { ?>
                        <option value="<?php print($i); ?>"><?php print($i); ?></option><?php } ?>
                </select>
                <br>



                Пол:<br />
                <label><input type="radio"  name="radio1" value="woman"  />
                    Женский</label>

                <label><input type="radio" name="radio1" value="man" />
                    Мужской</label><br />

                <label><input type="radio" name="radio1" value="nogender" />
                    Не определился</label><br />


                Сколько у вас конечностей :<br />
                <label><input type="radio" name="radio2" value="0" />  0</label>
                <label><input type="radio" name="radio2" value="1" /> 1</label>
                <label><input type="radio" name="radio2" value="2" /> 2</label>
                <label><input type="radio"name="radio2" value="3" />3</label>
                <label><input type="radio" name="radio2" value="4" /> 4</label>
                <label><input type="radio" name="radio2" value="5" /> 5</label><br>

                <label>
                    Список сверхспособностей :
                    <br />
                    <select id="sp" name="super[]" multiple="multiple">
                        <option value="1">Левитация</option>
                        <option value="2">Телепатия</option>
                        <option value="3">Видеть будущее  </option>
                        <option value="4">Сдать дискретку на 5</option>
                    </select>
                </label>
                <br />

                <label>
                    Биография:<br />
                    <textarea name="bio" <?php if ($errors['bio']) {print 'class="error"';} ?> value="<?php print $values['bio']; ?>"></textarea></label><br />
                <br />
                <input type="checkbox" name="checkbox" value="1"/>
                С контрактом ознакомлен<br />
                <input type="submit" name="button" value="Отправить" />
            </form>
        </div>
    </div>
</div>

</body>
